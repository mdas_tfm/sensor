package client

import (
	"fmt"

	"gitlab.com/mdas_tfm/sensor/pkg/client/rest"
)

// Client interface defines methods for interacting with the GW
type Client interface {
	PostMeasure(body []byte) error
	RegisterSensor(body []byte) error
}

// NewClient returns the right Client implementation
func NewClient(clientType, host, port string) (Client, error) {
	// TODO(sjr) look at making this to happen by using init()
	if clientType == "REST" {
		client := rest.NewGWClient(host, port)
		return client, nil
	}
	return nil, fmt.Errorf("Wrong client type")
}
