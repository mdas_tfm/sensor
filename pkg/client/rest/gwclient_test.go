// +build integration !unit

package rest

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/mdas_tfm/sensor/pkg/sensor"
)

func TestClientPostRegisterOrMeasure(t *testing.T) {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		return
	})

	ts := httptest.NewServer(handler)
	defer ts.Close()
	client := ts.Client()
	gw := &GWClient{
		measureURL:  ts.URL + "/measure",
		registerURL: ts.URL + "/register",
		contentType: "application/json",
		client:      client,
	}
	metadata := &sensor.Metadata{
		ID: "deadbeef-deadbeef",
	}
	m := &sensor.Measure{
		Temperature: 24.5,
		TakenAt:     "2019-10-12T07:20:50.52Z",
		Metadata:    metadata,
	}
	body, _ := json.Marshal(metadata)
	err := gw.RegisterSensor(body)
	if err != nil {
		t.Fatalf("Error registering sensor: %v", err)
	}
	body, _ = json.Marshal(m)
	err = gw.PostMeasure(body)
	if err != nil {
		t.Fatalf("Error sending measure: %v", err)
	}
}
