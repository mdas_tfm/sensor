package sensor

import "testing"

func TestSensorGeneratesMeasure(t *testing.T) {
	s, _ := NewSensor(
		"127.0.0.1",
		"8080",
		"test_area",
		"41.40,2.16",
		false,
	)

	m := s.TakeSnap()
	if m == nil {
		t.Error("Expected Measure but found nil")
	}

	if m.Metadata.Area != "test_area" {
		t.Errorf("measure metadata area: %s", m.Metadata.Area)
	}

	if m.Metadata.Location.Latitude != 41.40 {
		t.Errorf("measure metadata latitude: %.02f", m.Metadata.Location.Latitude)
	}

	if m.Metadata.Location.Longitude != 2.16 {
		t.Errorf("measure metadata longitude: %.02f", m.Metadata.Location.Longitude)
	}
}
