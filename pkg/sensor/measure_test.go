package sensor

import (
	"math/rand"
	"testing"
	"time"
)

func TestAMeasure(t *testing.T) {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	m := newMeasure(r)

	if m == nil {
		t.Error("Measure can't be nil")
	}

	if _, err := time.Parse(time.RFC3339, m.TakenAt); err != nil {
		t.Errorf("TakenAt could not be parsed: %s", m.TakenAt)
	}
	now := time.Now()
	month := now.Month().String()
	baseTemp := meanByMonth[month]
	// NOTE: max/min are crude approximations, it's probably safe
	//       and should keep the values on a decent range
	if ((baseTemp - 10.0) > m.Temperature) || (m.Temperature > (baseTemp + 10.0)) {
		t.Errorf("Temperature out of range: %.02f", m.Temperature)
	}
}
