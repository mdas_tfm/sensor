package sensor

import (
	"math/rand"
	"time"
)

var meanByMonth = map[string]float64{
	"January":   5,
	"February":  10,
	"March":     10,
	"April":     15,
	"May":       25,
	"June":      30,
	"July":      35,
	"August":    30,
	"September": 25,
	"October":   20,
	"November":  10,
	"December":  5,
}

func newMeasure(r *rand.Rand) *Measure {
	m := new(Measure)
	w := new(Metadata)
	l := new(location)
	m.simulateMeasure(r)
	m.Metadata = w
	m.Metadata.Location = l
	return m
}

func (m *Measure) simulateMeasure(r *rand.Rand) {
	now := time.Now()
	month := now.Month().String()
	baseTemp := meanByMonth[month]
	h := now.Hour()

	if h < 13 {
		m.Temperature = (float64(h)-13.0)*r.Float64() + baseTemp
	} else {
		m.Temperature = (13.0-float64(h))*r.Float64() + baseTemp
	}
	m.TakenAt = now.UTC().Format(time.RFC3339)
}
