package sensor

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"github.com/sirupsen/logrus"
)

var (
	measuresCounter = promauto.NewCounter(prometheus.CounterOpts{
		Name: "sensor_taken_measurements_total",
		Help: "The total number of taken measurements",
	})
)

// NewSensor initializes the sensor simulator
func NewSensor(GWAddress, GWPort, Name, GeoLoc string, debug bool) (*Sensor, error) {
	// FIXME(sjr) add validation
	log := logrus.New()
	log.Out = os.Stdout
	log.Formatter = &logrus.JSONFormatter{}
	loc := strings.Split(GeoLoc, ",")
	lat, err := strconv.ParseFloat(loc[0], 64)
	if err != nil {
		log.Errorf("Error parsing latitude: %v\n", err)
		return nil, err
	}

	lon, err := strconv.ParseFloat(loc[1], 64)
	if err != nil {
		log.Errorf("Error parsing longitude: %v\n", err)
		return nil, err
	}
	w := &Metadata{
		genID(Name),
		Name,
		&location{lat, lon},
	}
	r := rand.New(rand.NewSource(time.Now().Unix()))
	s := &Sensor{
		GWAddress,
		GWPort,
		r,
		log,
		w,
	}

	if debug {
		s.log.Level = logrus.DebugLevel
	}

	s.log.Debugf("sensor name: %s", Name)
	s.log.Debugf("sensor location: %s", GeoLoc)
	s.log.Debugf("sensor GW: %s:%s", GWAddress, GWPort)
	s.log.Debugf("sensor ID: %s", s.ID)
	s.log.Debugf("sensor initialized")
	return s, nil
}

// TakeSnap returns the current temperature
func (s *Sensor) TakeSnap() *Measure {
	m := newMeasure(s.R)
	m.Metadata = s.Metadata
	measuresCounter.Inc()
	return m
}

// HealthHandler reports the overall sensor Health
func (s *Sensor) HealthHandler(w http.ResponseWriter, r *http.Request) {
	// FIXME(sjr) This should be better handled on a goroutine and results
	//            stored a shared data structure. For now, this will have to do.
	statusCode := http.StatusOK
	message := "All components running."
	timeout := 5 * time.Second
	connStr := fmt.Sprintf("%s:%s", s.GWAddress, s.GWPort)
	conn, err := net.DialTimeout("tcp", connStr, timeout)
	if err != nil {
		statusCode = http.StatusInternalServerError
		message = fmt.Sprintf("Gateway is unreachable: %v", err)
	} else {
		defer conn.Close()
	}
	data := struct {
		Message    string
		StatusCode int
	}{
		message,
		statusCode,
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(data)
}

func genID(a string) string {
	h := sha1.New()
	h.Write([]byte(a))
	return fmt.Sprintf("%x", h.Sum(nil))
}
