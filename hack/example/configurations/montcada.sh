#!/bin/bash

export SENSOR_NAMESPACE="tfm"
export SENSOR_AREA_NAME="Montcada"
export SENSOR_GW_ADDRESS="gateway.tfm.svc.cluster.local"
export LOCATION="41.487222,2.187778"
export SENSOR_INTERVAL="5s"
export SENSOR_METRICS_PORT="2112"