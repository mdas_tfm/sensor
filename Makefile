GOCMD = go
DOCKERCMD = docker
CONTAINERBUILD = ${DOCKERCMD} build .
CONTAINERPUSH = ${DOCKERCMD} push
CONTAINERRM = ${DOCKERCMD} rmi -f
CONTAINERIMG = mdas2020/sensor
GOBUILD = ${GOCMD} build
GOCLEAN = ${GOCMD} clean
GOTEST = ${GOCMD} test
GOVET = ${GOCMD} vet
GOFMT = ${GOCMD} fmt
BINARY_NAME = sensor
GIT_VERSION := "$(shell git describe --abbrev=4 --dirty --always --tags)"

all: tests build

fetch:
	GO111MODULE="off" $(GOCMD) get -u github.com/jstemmer/go-junit-report
build:
	$(GOBUILD) -o ${BINARY_NAME} -ldflags "-X main.Version=${GIT_VERSION}" -v

container:
	$(CONTAINERBUILD) --build-arg VERSION=${GIT_VERSION} -t ${CONTAINERIMG}:${GIT_VERSION}

push:
	$(CONTAINERPUSH) ${CONTAINERIMG}:${GIT_VERSION}

clean:
	$(GOCLEAN)

lint:
	$(GOFMT) ./...
	$(GOVET) ./...

tests: fetch
	$(GOTEST) -timeout 30s -v ./... | go-junit-report -set-exit-code > unit.xml

fulltests: fetch
	$(GOTEST) -timeout 30s -tags=integration -v ./... | go-junit-report -set-exit-code > integration.xml

purge: clean
	$(CONTAINERRM) ${CONTAINERIMG}

.PHONY: all build container clean lint purge tests
