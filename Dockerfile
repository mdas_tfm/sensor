FROM golang:1.14-alpine AS builder
ARG VERSION
WORKDIR /tmp/sensor/
COPY . .
RUN apk --no-cache add make git && make build
FROM alpine:latest
ARG VERSION
LABEL org.mdas.component=sensor
LABEL org.mdas.component.version=$VERSION
LABEL org.mdas.description="Temperature sensor simulator"
RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /tmp/sensor/sensor .
RUN chmod -x ./sensor
CMD ["./sensor"]
